module.exports = {
    root: true,
    env: {
        browser: true,
        node: true
    },
    parserOptions: {
        parser: "babel-eslint"
    },
    extends: [
        "@nuxtjs",
        "plugin:nuxt/recommended"
    ],
    plugins: [],
    // add your custom rules here
    rules: {
        "vue/html-self-closing": "off",
        "no-return-assign": "off",
        "comma-dangle": 0,
        indent: ["error", 4],
        semi: [
            "error",
            "always"
        ],
        quotes: [
            "error",
            "double",
            {
                allowTemplateLiterals: true
            }
        ],
        // https://eslint.org/docs/rules/object-curly-spacing
        "object-curly-spacing": [
            "error",
            "always"
        ],
        // https://eslint.org/docs/rules/brace-style
        "brace-style": [
            "error",
            "1tbs"
        ],
        // https://eslint.org/docs/rules/curly
        curly: [
            "error",
            "all"
        ]
    }
};
