import axios from "axios";
export const state = () => ({
    photos: [],
    isLoading: false
});

export const getters = {
    getPhotos: (state) => {
        return state.photos;
    },
    isLoading: (state) => {
        return state.isLoading;
    }
};

export const actions = {
    async refreshPhotos ({
        commit
    }) {
        state.isLoading = true;
        await axios.get("https://hiren--devs.ew.r.appspot.com/gift-palace-photos")
            .then(response => commit("setPhotos", response.data));
    }
};

export const mutations = {
    setPhotos (state, photos) {
        state.photos = photos;
        state.isLoading = false;
    }
};
